
## Chào hỏi đưa tên
* greet
  - utter_greet
* give_name
  - utter_greet_with_name
* ask_name
  - utter_ask_name
* goodbye
  - utter_bye

## Đưa tên luôn
* give_name
  - utter_greet_with_name

## Chào - tên - hỏi chức năng - chào
* greet
  - utter_greet
* ask_name
  - utter_ask_name
* ask_func_list
  - utter_func_list
* goodbye
  - utter_bye
  
## Chào  - hỏi chức năng - chào
* greet
  - utter_greet
* ask_func_list
  - utter_func_list
* goodbye
  - utter_bye

## Chào  - hỏi tên - chào
* greet
  - utter_greet
* ask_name
  - utter_ask_name
* goodbye
  - utter_bye

## Hỏi tên - hỏi chức năng
* ask_name
  - utter_ask_name
* ask_func_list
  - utter_func_list

## Cảm ơn
* thank
  - utter_thank

## bất lịch sự
* lost_mess
  - utter_lost_mess

## uneti
* uneti
  - utter_uneti 

## hỏi về Hãng dell
* ask_customer_dell
 - utter_ask_customer_dell

## hỏi về Hãng HP
* ask_customer_hp
 - utter_ask_customer_hp

## hỏi về Hãng Apple
* apple
 - utter_apple

 ## hỏi về Hãng Microsoft
* microsoft
 - utter_microsoft

 ## hỏi về Hãng pc
* pc
 - utter_pc