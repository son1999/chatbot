## intent:give_name
- [Anh](cust_sex) là [Thắng](cust_name)
- [Anh](cust_sex) là [Sơn](cust_name)
- [Anh](cust_sex) là [Dương](cust_name)
- [Anh](cust_sex) là [Phú](cust_name)
- [Anh](cust_sex) là [Khoa](cust_name)
- [Anh](cust_sex) là [Hùng](cust_name)
- [Anh](cust_sex) là [Chiến](cust_name)
- [Anh](cust_sex) là [Trừng](cust_name)
- [Anh](cust_sex) là [Hải](cust_name)
- [Anh](cust_sex) là [Minh](cust_name)
- [Anh](cust_sex) là [Việt](cust_name)
- [Anh](cust_sex) là [Tuấn](cust_name)
- [Anh](cust_sex) là [Quang](cust_name)
- [Anh](cust_sex) là [Tuấn Anh](cust_name)
- [Chị](cust_sex) là [Phương Anh](cust_name)
- [Chị](cust_sex) là [Hà](cust_name)
- [Chị](cust_sex) là [Hiếu](cust_name)
- [Chị](cust_sex) là [Oanh](cust_name)
- [Chị](cust_sex) là [Vi](cust_name)
- [Chị](cust_sex) là [Thư](cust_name)
- [Chị](cust_sex) là [Quỳnh](cust_name)
- [Em](cust_sex) là [Trang](cust_name)
- [Cô](cust_sex) [Vân](cust_name)
- [Chú](cust_sex) [Hùng](cust_name)

## intent:greet
- xin chào
- chào bạn
- hello
- hi
- hey
- mình có vài câu hỏi
- có ai giúp mình không
- bạn ơi
- anh ơi
- chị ơi
- cháu ơi
- chào 
- có ai ở đây không? 
- có ai ở đây không
- có ai chat được không? 
- có ai chat được không
- chat

## intent:goodbye
- tạm biệt
- chào tạm biệt
- chào tạm biệt em
- tạm biệt em
- tạm biệt em nhé

## intent:thank
- chuẩn
- hay
- siêu
- tuyệt
- cám ơn
- thanks
- thank you
- ok
- cảm ơn em
- cảm ơn em nhiều nhé
- cảm ơn

## intent:ask_func_list
- bạn có thể làm được những gì
- bạn giúp được gì nào
- chức năng của bạn là gì
- bạn có thể làm được mấy chức năng
- bạn giỏi nhất làm gì
- bạn có tư vấn giúp mình được không
- việc gì bạn làm được
- kể xem bạn làm được gì
- cho mình biết bạn làm được gì nhé
- bạn hữu dụng như thế nào
- bạn có ích trong những việc gì
- lĩnh vực gì bạn giỏi nhất
- mình tò mò về những việc bạn làm được
- chẳng biết bạn làm được gì
- bạn tệ nhất trong việc gì
- bạn biết được những lĩnh vực gì
- bạn giỏi hỗ trợ nhất trong lĩnh vực gì
- kể cho mình biết những việc bạn có thể làm được nhé
- nói cho mình về những việc bạn giúp được mình
- bạn giúp được mình gì nào
- bạn có thể làm gì
- em giúp được gì
- em làm được gì
- mày biết làm gì
- em biết làm gì
- bạn biết làm gì
- biết làm gì
- làm được gì 
- có làm được gì 
- có làm được trò gì 
- giúp giúp cái gì
- biết gì mà nói
- làm được gì
- giúp được gì
- mày làm được gì
- có biết gì 

## intent:ask_customer_dell
- dell
- muốn mua máy tính hãng dell
- mua máy dell
- hãng dell
- máy tính dell

## intent:ask_customer_hp
- HP
- hp
- mua máy hp
- mua máy HP
- muốn mua máy tính hãng HP
- muốn mua máy tính hãng hp
- hãng hp
- máy tính hp
- mua hãng HP

## intent:ask_customer_laptop_all
- muốn mua máy Acer
- mua máy Acer
- muốn mua máy Lenovo
- mua máy Lenovo
- muốn mua máy Samsung
- mua máy Samsung
- muốn mua máy Asus
- mua máy Asus
- muốn mua máy MSI
- mua máy MSI
- muốn mua máy LG
- mua máy LG
- muốn mua máy Toshiba
- mua máy Toshiba
- muốn mua máy Alienware
- mua máy Alienware
- muốn mua máy Razer
- mua máy Razer
- muốn mua máy Xiaomi
- mua máy Xiaomi


## intent:ask_name
- bạn tên gì
- tên gì
- chị tên gì
- anh tên gì
- tên của bạn là gì
- tên của chị là gì
- tên của anh là gì
- cho mình hỏi tên nhé
- mình muốn biết tên bạn
- mình muốn tiện xưng hộ hơn, cho mình biết tên nhé
- để tiện xưng hộ thì tên bạn là gì
- cho em xin tên của anh ạ
- cho em xin tên của chị ạ
- được biết tên bạn thì tốt nhỉ
- vui lòng cho mình biết tên nhé
- xin phép hỏi tên của bạn
- vậy thì tên bạn là gì
- tên bạn trong tiếng việt là gì
- bạn có vui lòng cho mình biết tên được không
- chúng ta nên biết tên nhau nhỉ?
- em tên là gì thế
- tên em là gì
- tên em 
- tên bạn 
- bạn tên là gì 
- bạn tên gì thế
- em tên gì

## intent:lost_mess
- mày ngu lắm
- dốt
- đần 

## intent:uneti
- uneti
- Đại học kinh tế kỹ thuật công nghiệp
- Đại học kinh tế kĩ thuật công nghiệp
- dai hoc kinh te ki thuat cong nghiep
- dai hoc kinh te ky thuat cong nghiep

## intent:apple
- mua máy Mac
- mua máy IMac
- mua máy Iphone
- mua máy Macbook
- mua máy Mac mini
- mua máy apple
- mua máy Apple

## intent:microsoft
- mua máy Surface 
- máy Surface 

## intent:pc
- mua máy Pc
- máy Pc
- mua máy PC
- máy PC
- mua máy pc
- máy pc
